create table pixels
(
    id bigint GENERATED ALWAYS AS IDENTITY,
    nome text not null,
    x integer not null,
    y integer not null,
    r integer not null,
    g integer not null,
    b integer not null,
    alpha integer not null,

    constraint pixels_unique_pkey primary key (nome, x, y) ,
    constraint pixels_unique unique ( id ) 
);