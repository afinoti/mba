create table corrida
(
	id bigint GENERATED ALWAYS AS IDENTITY,
	nome text not null,
	dia integer not null,
	tempo numeric not null,
	
    constraint corrida_unique_pkey primary key (nome, dia, tempo) ,
    constraint corrida_unique unique ( id ) 
);

