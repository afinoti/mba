
-- FUNCTION: public.euclideandist(anyarray, anyarray)

-- DROP FUNCTION public.euclideandist(anyarray, anyarray);

CREATE OR REPLACE FUNCTION public.euclideandist(
	a anyarray,
	b anyarray)
    RETURNS double precision
    LANGUAGE 'plpgsql'
    COST 100
    IMMUTABLE STRICT PARALLEL UNSAFE
AS $BODY$
DECLARE
Dim INT;
Tot DOUBLE PRECISION:=0.0;
BEGIN
Dim:=LEAST(array_length (A,1), array_length (B,1));
FOR i IN 1..Dim LOOP
Tot:=Tot+(A[i]-B[i])^2;
END LOOP;
RETURN SQRT(Tot);
END;
$BODY$;

ALTER FUNCTION public.euclideandist(anyarray, anyarray)
    OWNER TO postgres;
