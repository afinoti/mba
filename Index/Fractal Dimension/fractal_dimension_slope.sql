
WITH Grid AS (SELECT I.x, I.y, I.r, J.x, J.y, J.r,
	EuclideanDist(ARRAY[ I.x, I.y, I.r], ARRAY[ J.x, J.y, J.r]) Dist
	FROM pixels I CROSS JOIN pixels J
	where I.nome like 'texto%'
	and  I.r =0
	and J.nome like 'texto%'			  
	and  J.r =0			  
	--WHERE < TuplasAConsiderar >
	),
	Contagem AS (SELECT Dist,
	ROUND(256*Percent_Rank() OVER (ORDER BY Dist)) Pos --> 7 divis~oes
	FROM Grid ),
	DistExp AS (SELECT Pos, Avg(Dist) ADist, Count(Pos),
	Sum(Count(Pos)) OVER (ORDER BY Pos ASC
	ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) PDF
	FROM Contagem
	WHERE Dist>0 AND Pos>0
	GROUP BY Pos
	ORDER BY Pos)
	SELECT regr_slope(Log(Pos), Log(PDF)) Slope, --> Dimens~ao Fractal
	regr_intercept(Log(Pos), Log(PDF)) Intercept
	FROM DistExp ;